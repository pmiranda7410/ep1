#include "candidato.hpp"
#include <string>
#include<fstream>
#include<iostream>

using namespace std;

Candidato::Candidato(){
   SG_UE = "";
   cargo = "";
   numero_candidato = "";
   nome_candidato = "";
   nome_urna="";
   partido = "";
   bool candidato_inexistente = false;


}

string Candidato::get_SG_UE(){
  return SG_UE;
}
void Candidato::set_SG_UE(string SG_UE){
  this->SG_UE = SG_UE;
}
string Candidato::get_cargo(){
  return cargo;
}
void Candidato::set_cargo(string cargo){
  this->cargo = cargo;
}
string Candidato::get_numero_candidato(){
  return numero_candidato;
}
void Candidato::set_numero_candidato(string numero_candidato){
  this->numero_candidato = numero_candidato;
}
string Candidato::get_nome_candidato(){
  return nome_candidato;
}
void Candidato::set_nome_candidato(string nome_candidato){
  this->nome_candidato= nome_candidato;
}
string Candidato::get_nome_urna(){
  return nome_urna;
}
void Candidato::set_nome_urna(string nome_urna){
  this->nome_urna = nome_urna;
}
string Candidato::get_partido(){
  return partido;
}
void Candidato::set_partido(string partido){
  this->partido = partido;
}
bool Candidato::get_candidato_inexistente(){
  return candidato_inexistente;
}
void Candidato::set_candidato_inexistente(bool candidato_inexistente){
  this-> candidato_inexistente = candidato_inexistente;
}


void Candidato::dados_candidato_selecionado(string escolha_eleitor){

      string trash;
        ifstream file_BR;
        file_BR.open("data/consulta_cand_2018_BR.csv");
        if(!file_BR.is_open()) std::cout << "Error: File Open" << endl;
    // ifstream ip("consulta_cand_2018_BR.csv");
      //if(!ip.is_open()) std::cout << "Error: File Open" << endl;
      bool candidato_inexistente = false;
      int j = 0;
      numero_candidato = "";
      SG_UE = "";
      cargo = "";
      nome_candidato = "";
      partido = "";

        while(file_BR.good() && (escolha_eleitor != numero_candidato)){

          for( j = 0; j <11 ; j++){
            getline(file_BR, trash, ';');
          }

            getline(file_BR,SG_UE,';');

        for(j=0; j < 2; j++){
          getline(file_BR, trash, ';');
        }
          getline(file_BR,trash,'"');
          getline(file_BR,cargo,'"');
          getline(file_BR, trash, ';');
          for( j = 0; j <1 ; j++){
            getline(file_BR, trash, ';');
          }
            j=0;
            getline(file_BR,trash,'"');
            getline(file_BR,numero_candidato,'"');
            getline(file_BR,trash,';');
            getline(file_BR, nome_candidato, ';');
            getline(file_BR, nome_urna, ';');
            for(j=0; j< 8; j++){
              getline(file_BR, trash, ';');
            }
            getline(file_BR, trash, ';');
            getline(file_BR, trash, ';');
            getline(file_BR, partido, ';');
            getline(file_BR, trash, '\n');
          }

      file_BR.close();


      ifstream ip("data/consulta_cand_2018_DF.csv");
      if(!ip.is_open()) std::cout << "Error: File Open" << endl;

      while(ip.good() && (escolha_eleitor != numero_candidato)){

        for( j = 0; j <11 ; j++){
          getline(ip, trash, ';');
        }
        j=0;
        getline(ip,SG_UE,';');

        for(j=0; j < 2; j++){
          getline(ip, trash, ';');
        }
          getline(ip,trash,'"');
          getline(ip,cargo,'"');
          getline(ip, trash, ';');
        for( j = 0; j <1 ; j++){
          getline(ip, trash, ';');
        }
          j=0;
          getline(ip,trash,'"');
          getline(ip,numero_candidato,'"');
          getline(ip,trash,';');
          getline(ip, nome_candidato, ';');
          getline(ip, nome_urna, ';');
          for(j=0; j< 8; j++){
            getline(ip, trash, ';');
          }
          getline(ip, trash, ';');
          getline(ip, trash, ';');
          getline(ip, partido, ';');
          getline(ip, trash, '\n');
      }

      if(escolha_eleitor == numero_candidato){
        cout << "-------------------------" << endl << "Nome do candidato: " <<  nome_candidato << endl;
        cout << "Nome de Urna: " << nome_urna << endl;
        cout << "SG_UE: " <<  SG_UE << endl;
        cout << "Numero Candidato: " << numero_candidato << endl;
        cout << "Cargo: " << cargo << endl;
        cout << "Partido: " << partido << endl;
        cout << "-------------------------" << endl;
        set_candidato_inexistente(false);
      }
    else{
      cout << endl << "      ---- ERRO -- Inserido valor ou caracter inválido. ----" << endl << endl;
      set_candidato_inexistente(true);

    }
      ip.close();

}




void Candidato::dados_candidato_selecionado(string escolha_eleitor, string cargo_escolha){



    string trash;
      ifstream file_BR;
      file_BR.open("data/consulta_cand_2018_BR.csv");
      if(!file_BR.is_open()) std::cout << "Error: File Open" << endl;
  // ifstream ip("consulta_cand_2018_BR.csv");
    //if(!ip.is_open()) std::cout << "Error: File Open" << endl;
    bool candidato_inexistente = false;
    int j = 0;
    numero_candidato = "";
    SG_UE = "";
    cargo = "";
    nome_candidato = "";
    partido = "";

      while(file_BR.good()){

        for( j = 0; j <11 ; j++){
          getline(file_BR, trash, ';');
        }

          getline(file_BR,SG_UE,';');

      for(j=0; j < 2; j++){
        getline(file_BR, trash, ';');
      }
        getline(file_BR,trash,'"');
        getline(file_BR,cargo,'"');
        getline(file_BR, trash, ';');
        for( j = 0; j <1 ; j++){
          getline(file_BR, trash, ';');
        }
          j=0;
          getline(file_BR,trash,'"');
          getline(file_BR,numero_candidato,'"');
          getline(file_BR,trash,';');
          getline(file_BR, nome_candidato, ';');
          getline(file_BR, nome_urna, ';');
          for(j=0; j< 8; j++){
            getline(file_BR, trash, ';');
          }
          getline(file_BR, trash, ';');
          getline(file_BR, trash, ';');
          getline(file_BR, partido, ';');
          getline(file_BR, trash, '\n');
          if(cargo == cargo_escolha && escolha_eleitor == numero_candidato){
            break;
          }
        }

    file_BR.close();


    ifstream ip("data/consulta_cand_2018_DF.csv");
    if(!ip.is_open()) std::cout << "Error: File Open" << endl;

    while(ip.good()){
      if(cargo == cargo_escolha && escolha_eleitor == numero_candidato){
        break;
      }

      for( j = 0; j <11 ; j++){
        getline(ip, trash, ';');
      }
      j=0;
      getline(ip,SG_UE,';');

      for(j=0; j < 2; j++){
        getline(ip, trash, ';');
      }
        getline(ip,trash,'"');
        getline(ip,cargo,'"');
        getline(ip, trash, ';');
      for( j = 0; j <1 ; j++){
        getline(ip, trash, ';');
      }
        j=0;
        getline(ip,trash,'"');
        getline(ip,numero_candidato,'"');
        getline(ip,trash,';');
        getline(ip, nome_candidato, ';');
        getline(ip, nome_urna, ';');
        for(j=0; j< 8; j++){
          getline(ip, trash, ';');
        }
        getline(ip, trash, ';');
        getline(ip, trash, ';');
        getline(ip, partido, ';');
        getline(ip, trash, '\n');
    }

    if(escolha_eleitor == numero_candidato){
      cout << "-------------------------" << endl << "Nome do candidato: " <<  nome_candidato << endl;
      cout << "Nome de Urna: " << nome_urna << endl;
      cout << "SG_UE: " <<  SG_UE << endl;
      cout << "Numero Candidato: " << numero_candidato << endl;
      cout << "Cargo: " << cargo << endl;
      cout << "Partido: " << partido << endl;
      cout << "-------------------------" << endl;
      set_candidato_inexistente(false);
    }
  else{
    cout << endl << "      ---- ERRO -- Inserido valor ou caracter inválido. ----" << endl << endl;
    set_candidato_inexistente(true);

  }
    ip.close();

}


//listar todos os candidatos por cargo
void Candidato::lista_candidatos_por_cargo(string cargo_lista){

  string trash;
    ifstream file_BR;
    file_BR.open("data/consulta_cand_2018_BR.csv");
    if(!file_BR.is_open()) std::cout << "Error: File Open" << endl;
// ifstream ip("consulta_cand_2018_BR.csv");
  //if(!ip.is_open()) std::cout << "Error: File Open" << endl;
  bool candidato_inexistente = false;
  int j = 0;
  string numero_candidato = "";
  string nome_urna = "";
  string SG_UE = "";
  string cargo_confere = "";
  string nome_candidato = "";
  string partido = "";



    while(file_BR.good()){

      for( j = 0; j <11 ; j++){
        getline(file_BR, trash, ';');
      }

        getline(file_BR,SG_UE,';');

    for(j=0; j < 2; j++){
      getline(file_BR, trash, ';');
    }
      getline(file_BR,trash,'"');
      getline(file_BR,cargo_confere,'"');
      getline(file_BR, trash, ';');
      for( j = 0; j <1 ; j++){
        getline(file_BR, trash, ';');
      }
        j=0;
        getline(file_BR,trash,'"');
        getline(file_BR,numero_candidato,'"');
        getline(file_BR,trash,';');
        getline(file_BR, nome_candidato, ';');
        getline(file_BR, nome_urna, ';');
        for(j=0; j< 8; j++){
          getline(file_BR, trash, ';');
        }
        getline(file_BR, trash, ';');
        getline(file_BR, trash, ';');
        getline(file_BR, partido, ';');
        getline(file_BR, trash, '\n');
        if(cargo_lista == cargo_confere){
        cout << "Nome do candidato: " <<  nome_candidato;
        cout << endl << "Cargo: " << cargo_confere;
        cout <<  endl << "Numero Candidato: " << numero_candidato << endl << "-------------------" <<endl;
      }
      }

  file_BR.close();


  ifstream ip("data/consulta_cand_2018_DF.csv");
  if(!ip.is_open()) std::cout << "Error: File Open" << endl;

  while(ip.good()){

    for( j = 0; j <11 ; j++){
      getline(ip, trash, ';');
    }
    j=0;
    getline(ip,SG_UE,';');

    for(j=0; j < 2; j++){
      getline(ip, trash, ';');
    }
      getline(ip,trash,'"');
      getline(ip,cargo_confere,'"');
      getline(ip, trash, ';');
    for( j = 0; j <1 ; j++){
      getline(ip, trash, ';');
    }
      j=0;
      getline(ip,trash,'"');
      getline(ip,numero_candidato,'"');
      getline(ip,trash,';');
      getline(ip, nome_candidato, ';');
      getline(ip, nome_urna, ';');
      for(j=0; j< 8; j++){
        getline(ip, trash, ';');
      }
      getline(ip, trash, ';');
      getline(ip, trash, ';');
      getline(ip, partido, ';');
      getline(ip, trash, '\n');
      if(cargo_lista == cargo_confere){
      cout << "Nome do candidato: " <<  nome_candidato;
      cout << endl << "Numero Candidato: " << numero_candidato << endl << "-------------------" <<endl;
    }
  }

    cout << "                **** ^^^^ LISTA DE (" << cargo_lista << ") ACIMA ^^^^ ****" << endl;

  ip.close();


}
