#include <string>
#include <iostream>
#include <fstream>
#include "candidato.hpp"



using namespace std;

//URNA ELETRÔNICA
int main(int argc, char ** agrv){

string data;
string arquivo;
char iniciar = 'n';
char confirmar = 'n';
int votantes;
int i = 0; //Variável para loop
int j=0;
string opcao_menu_inicial = "0"; // define o que fazer no opcao_menu_inicial
string ficar_ou_voltar = "";


//Denominação do nome do arquivo
  cout << "---- CONFIGURAÇÃO DO ADMINISTRADOR ----" << endl;
  cout << "Coloque a Data de Hoje separando os dias,meses e ano por um _, por favor:" << endl;
  cout << "Nome do arquivo (dia_mês_ano): ";
  cin >> data;
  cout << endl << "Número de Eleitores Hoje: ";
  cin >> votantes;

  Candidato *candidato_selecionado = new Candidato();
  candidato_selecionado->abertura_arquivo_csv_contra_erro();



//presidente, governador, senador, deputado estadual e deputado distrital
  string nome;
  string candidato_presidente = "";
  string candidato_governador = "";
  string candidato_senador = "";
  string candidato_deputado_estadual = "";
  string candidato_deputado_distrital = "";

//Aqui será criado o arquivo com os dados dos votantes do dia
ofstream myfile;
myfile.open (data.c_str());
myfile << data << endl << endl;

//Nesse Loop será inserido no arquivo os dados de cada eleitor
while(i < votantes){
    i++;
    myfile << i << endl;

    //loop para determinar se o eleitor quer voltar ou continuar
     while(confirmar != 's'){
  cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
  cout << " ------------------------------ " << endl;
  cout << "|         Olá Eleitor!         |" <<  endl;
  cout << "|  Digite a opção de ação que  |" << endl;
  cout << "|       deseja realizar.       |" << endl;
  cout << "| (1) Descrição sobre Candidato|" << endl;
  cout << "| (2) Votar                    |" << endl;
  cout << " ------------------------------ " << endl;
  cout << "Digite --> ";
  cin.clear();
  cin >> opcao_menu_inicial;
  while(opcao_menu_inicial == "1" || opcao_menu_inicial != "2"){

    if(opcao_menu_inicial == "1"){
          while(ficar_ou_voltar != "x"){
                  cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
                  cout << "Para VOLTAR Digite (x) seguido do botão CONFIRMA(enter)." << endl;
                  cout << endl << "Digite o numero do candidato para obter informações:" << endl;
                  if(ficar_ou_voltar != "" && ficar_ou_voltar != "x"){
                    candidato_selecionado->dados_candidato_selecionado(ficar_ou_voltar);
                    ficar_ou_voltar = "";
                  }
                  getline(cin,ficar_ou_voltar);
                  if (ficar_ou_voltar == "x" || ficar_ou_voltar == "X"){
                    ficar_ou_voltar = "";
                    break;
                  }

                  cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
            }
          opcao_menu_inicial = "0";

    }
    cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
    cout << " ------------------------------ " << endl;
    cout << "|         Olá Eleitor!         |" <<  endl;
    cout << "|  Digite a opção de ação que  |" << endl;
    cout << "|       deseja realizar.       |" << endl;
    cout << "| (1) Descrição sobre Candidato|" << endl;
    cout << "| (2) Votar                    |" << endl;
    cout << " ------------------------------ " << endl << endl;
    cout << "Digite um dos valores acima, por favor." << endl;
    cout << "Digite -->";
    cin.clear();
    cin >> opcao_menu_inicial;


  if(opcao_menu_inicial == "2"){
      break;
   }
}

//Limpagem de Buffer e Digitação do nome do eleitor
 cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
 cout << i << endl << endl;

    while(confirmar != 's'){
           cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
         cout << "Para VOLTAR, ao menu, Digite (x) seguido do botão CONFIRMA(enter)." << endl;
         cout << endl << "Digite Seu nome: ";
         cin.clear();
         cin.ignore();
         getline(cin,nome);

         if (nome == "x" || nome == "X"){
           nome = "";
           break;
         }

         else{
        cout << endl << "Confirmar? (s/n): ";
        cin >> confirmar;
        }

    }
    confirmar = 'n';
while(nome != "x" && confirmar != 's'){
    confirmar = 'n';
    //Presidente
    while(confirmar != 's'){
       cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
         candidato_presidente = "PRESIDENTE";
         candidato_selecionado->lista_candidatos_por_cargo(candidato_presidente);
         cout << endl << "Para VOLTAR ao menu. Digite (x) e aperte CONFIRMAR(enter). " << endl << endl;
         cout << "**** Para votar NULO digite 'nulo' ****" << endl;
         cout << endl << "Digite o Número do Presidente que deseja votar: ";
         cin >> candidato_presidente;

         if(candidato_presidente == "x" || candidato_presidente == "X"){

           break;
         }

         else{
           if(candidato_presidente == "nulo"){
             cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
             cout << "Confirmar? (s/n): ";
             cin  >> confirmar;
           }
           else{
           candidato_selecionado->dados_candidato_selecionado(candidato_presidente, "PRESIDENTE");
                 if(candidato_selecionado->get_candidato_inexistente() == false){
                    cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                    cout << "Confirmar? (s/n): ";
                    cin  >> confirmar;
                  }
                  else{
                    candidato_presidente = "nulo";
                 cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                 cout << "Confirmar? (s/n): ";
                 cin  >> confirmar;
               }
         }

         }
       }
       confirmar = 'n';
       if(candidato_presidente == "x" || candidato_presidente == "X"){

         break;
       }

       //Governador
       while(confirmar != 's'){
          cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
          candidato_governador = "GOVERNADOR";
          candidato_selecionado->lista_candidatos_por_cargo(candidato_governador);
            cout << endl << "Para VOLTAR ao menu. Digite (x) e aperte CONFIRMAR(enter). " << endl << endl;
            cout << "**** Para votar NULO digite 'nulo' ****" << endl;
            cout << endl << "Digite o Número do Governador que deseja votar: ";
            cin >> candidato_governador;

            if(candidato_governador == "x" || candidato_governador == "X"){

              break;
            }

            else{
              if(candidato_governador == "nulo"){
                cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                cout << "Confirmar? (s/n): ";
                cin  >> confirmar;
              }
              else{
              candidato_selecionado->dados_candidato_selecionado(candidato_governador, "GOVERNADOR");
                    if(candidato_selecionado->get_candidato_inexistente() == false){
                       cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                       cout << "Confirmar? (s/n): ";
                       cin  >> confirmar;
                     }
                     else{
                       candidato_governador = "nulo";
                    cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                    cout << "Confirmar? (s/n): ";
                    cin  >> confirmar;
                  }
            }

            }
          }
          confirmar = 'n';
          if(candidato_governador == "x" || candidato_governador == "X"){

            break;
          }

        //SENADOR
        while(confirmar != 's'){
           cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
           candidato_senador = "SENADOR";
           candidato_selecionado->lista_candidatos_por_cargo(candidato_senador);
             cout << endl << "Para VOLTAR ao menu. Digite (x) e aperte CONFIRMAR(enter). " << endl << endl;
             cout << "**** Para votar NULO digite 'nulo' ****" << endl;
             cout << endl << "Digite o Número do Senador que deseja votar: ";
             cin >> candidato_senador;

             if(candidato_senador == "x" || candidato_senador == "X"){

               break;
             }

             else{
               if(candidato_senador == "nulo"){
                 cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                 cout << "Confirmar? (s/n): ";
                 cin  >> confirmar;
               }
               else{
               candidato_selecionado->dados_candidato_selecionado(candidato_senador, "SENADOR");
                         if(candidato_selecionado->get_candidato_inexistente() == false){
                            cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                            cout << "Confirmar? (s/n): ";
                            cin  >> confirmar;
                          }
                          else{
                            candidato_senador = "nulo";
                         cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                         cout << "Confirmar? (s/n): ";
                         cin  >> confirmar;
                       }
             }

             }
           }
           confirmar = 'n';
           if(candidato_senador == "x" || candidato_senador == "X"){

             break;
           }

           //Deputado FEDERAL
           while(confirmar != 's'){
              cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
              candidato_deputado_estadual = "DEPUTADO FEDERAL";
              candidato_selecionado->lista_candidatos_por_cargo(candidato_deputado_estadual);
                cout << endl << "Para VOLTAR ao menu. Digite (x) e aperte CONFIRMAR(enter). " << endl << endl;
                cout << "**** Para votar NULO digite 'nulo' ****" << endl;
                cout << endl << "Digite o Número do Deputado Federal que deseja votar: ";
                cin >> candidato_deputado_estadual;

                if(candidato_deputado_estadual == "x" || candidato_deputado_estadual == "X"){

                  break;
                }

                else{
                  if(candidato_deputado_estadual == "nulo"){
                    cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                    cout << "Confirmar? (s/n): ";
                    cin  >> confirmar;
                  }
                  else{
                  candidato_selecionado->dados_candidato_selecionado(candidato_deputado_estadual, "DEPUTADO FEDERAL");
                          if(candidato_selecionado->get_candidato_inexistente() == false){
                             cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                             cout << "Confirmar? (s/n): ";
                             cin  >> confirmar;
                           }
                           else{
                             candidato_deputado_estadual = "nulo";
                          cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                          cout << "Confirmar? (s/n): ";
                          cin  >> confirmar;
                        }
                }

                }
              }
              confirmar='n';
              if(candidato_deputado_estadual == "x" || candidato_deputado_estadual == "X"){

                break;
              }

            //Deputado DISTRITAL
            while(confirmar != 's'){
               cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";
               candidato_deputado_distrital = "DEPUTADO DISTRITAL";
               candidato_selecionado->lista_candidatos_por_cargo(candidato_deputado_distrital);
                 cout << endl << "Para VOLTAR ao menu. Digite (x) e aperte CONFIRMAR(enter). " << endl << endl;
                 cout << "**** Para votar NULO digite 'nulo' ****" << endl;
                 cout << endl << "Digite o Número do Deputado Distrital que deseja votar: ";
                 cin >> candidato_deputado_distrital;

                 if(candidato_deputado_distrital == "x" || candidato_deputado_distrital == "X"){

                   break;
                 }

                 else
                 {
                   if(candidato_deputado_distrital == "nulo"){
                     cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                     cout << "Confirmar? (s/n): ";
                     cin  >> confirmar;
                   }
                   else{
                   candidato_selecionado->dados_candidato_selecionado(candidato_deputado_distrital, "DEPUTADO DISTRITAL");
                        if(candidato_selecionado->get_candidato_inexistente() == false){
                           cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                           cout << "Confirmar? (s/n): ";
                           cin  >> confirmar;
                         }
                         else{
                           candidato_deputado_distrital = "nulo";
                         cout << endl << "Se for colocado um número inexistente ou diferente do cargo o voto será anulado" << endl;
                         cout << "Confirmar? (s/n): ";
                         cin  >> confirmar;
                       }
                 }
                 }
               }

               if(candidato_deputado_distrital == "x" || candidato_deputado_distrital == "X"){

                 break;
               }
}

}//fechar while de confirmação
  confirmar = 'n';

//Implementação dos dados do eleitor no relatório de votação
 myfile << "Nome do Eleitor: " << nome << endl;
 myfile << "Candidato Presidente: " << candidato_presidente << endl;
 myfile << "Candidato Governador: " << candidato_governador << endl;
 myfile << "Candidato Senador: " << candidato_senador << endl;
 myfile << "Candidato Deputado Federal: " << candidato_deputado_estadual << endl;
 myfile << "Candidato Deputado Distrital: " << candidato_deputado_distrital << endl;
 myfile << "-----------------------------------------------" << endl;

 cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";

//Início loop para próximo eleitor
 if(i == votantes){
   cout << "Obrigado por votar conosco!\a " << endl;
   break;
 }
 cout << "Obrigado! Passe para o próximo votante, por favor." << endl;
 cout << "Iniciar votação? (s/n) \a" << endl;
 while(iniciar != 's'){
   cin >> iniciar;
   if(iniciar != 's'){
      cout << "Iniciar votação? (s/n)" << endl;
   }
 }

 cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n";

}


 myfile.close();


return 0;

};
