#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP
#include<string>
#include<iostream>
#include<fstream>

using namespace std;

class Candidato{

private:
    string SG_UE;
    string cargo;
    string numero_candidato;
    string nome_candidato;
    string nome_urna;
    string partido;
    bool candidato_inexistente;

public:

  //Polimorfismo -- Sobrecarga;
  Candidato();
  Candidato(string numero_candidato);
  ~Candidato();

  string get_SG_UE();
  void set_SG_UE(string SG_UE);
  string get_cargo();
  void set_cargo(string cargo);
  string get_numero_candidato();
  void set_numero_candidato(string numero_candidato);
  string get_nome_candidato();
  void set_nome_candidato(string nome_candidato);
  string get_nome_urna();
  void set_nome_urna(string nome_urna);
  string get_partido();
  void set_partido(string partido);
  bool get_candidato_inexistente();
  void set_candidato_inexistente(bool candidato_inexistente);

void dados_candidato_selecionado(string escolha_eleitor, string cargo_escolha);
void dados_candidato_selecionado(string escolha_eleitor);

void lista_candidatos_por_cargo(string cargo_lista);

void abertura_arquivo_csv_contra_erro();


};


#endif
