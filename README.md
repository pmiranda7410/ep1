# Exercício de Programação 1

Acesse a descrição completa deste exercício na [wiki](https://gitlab.com/oofga/eps_2018_2/ep1/wikis/Descricao)

## Como usar o projeto

    Etapas Passo a passo nas Funcionalidades abaixo.

## Funcionalidades do projeto
Esse Projeto contém ás seguintes etapas:
    
Primeiro abre as opções do Adiministrador, no qual, vai colocar o nome do arquivo 
com a data do dia em que será feita a eleição, como por exemplo: "02_10_2018", 
que assim fica mais fácil de saber quando o arquivo foi criado.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Part1_adm.png)
        
Logo depois, aparecerá a opção de colocar o número de eleitores, que será rodado 
dentro de um while para cada eleitor até finalizar o total. Então é criado o arquivo 
do tipo .txt, cujo nome será a data inserida pelo ADM.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte2_Adm.png)
        
Após a parte do adiministrador ele vai seguir da seguinte forma:
    
(Em qualquer etapa a partir daqui, apertar "x" ou "X" irá para o "Menu Inicial" dos eleitores).

Irá aparecer o Menu Principal dos Eleitores, onde cada eleitor terá a opção "1" e "2".

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte3_Eleitor.png)

- Na opção "1" (Descrição do Candidato) => Colocando o Número de qualquer candidato, 
ele trará informações sobre tal candidato.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte4_Eleitor_DescricaoCandidato.png)

Exemplo, candidato do numero 44:

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte5_DescricaoCandidatoExemplo.png)

Apertando "X" e enter o programa te retornará para o Menu dos eleitores.

Seguindo para proxima etapa:

- Na opção "2" (Votar) => O eleitor vai para a opção de voto.
            
Na opção de Voto o Eleitor terá as seguintes etapas:
        
- 1º- O programa pedirá o nome do eleitor, seguido de um pedido de confirmação do nome, 
caso esteja errado, digitar "n" para escrever novamente.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte6_OpcaoVotarNome.png)
            
- 2º- o programa trará uma lista com todos os PRESIDENTES e você poderá ver e escolher 
qual deseja votar (caso esqueceu o número do candidato);
Seguido de um pedido de confirmação igual na etapa 1.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte7_primeiroVotoPresidente.png)
            
- 3º- O programa trará uma lista com todos os GOVERNADORES e você poderá ver e escolher 
qual deseja votar (caso esqueceu o número do candidato);
Seguido de um pedido de confirmação igual na etapa 1.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte8_ListaGovernadores.png)
                    
- 4º- O programa trará uma lista com todos os SENADORES e você poderá ver e escolher 
qual deseja votar (caso esqueceu o número do candidato);
Seguido de um pedido de confirmação igual na etapa 1.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte9_ListaSenadores.png)
                    
- 5º- O programa trará uma lista com todos os DEPUTADOS FEDERAIS e você poderá ver e escolher 
qual deseja votar (caso esqueceu o número do candidato);
Seguido de um pedido de confirmação igual na etapa 1.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte10_ListaDeputadosFederais.png)
                    
- 6º- O programa trará uma lista com todos os DEPUTADOS DISTRITAIS e você poderá ver e escolher 
qual deseja votar (caso esqueceu o número do candidato);
Seguido de um pedido de confirmação igual na etapa 1.

![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte11_ListaDeputadosDistritais.png)
            
Então caso você tenha confirmado todos, o programa escreverá todos os dados, nos quais foram salvos 
em variáveis simples criadas na main, para o arquivo .txt da eleição. Caso, você tenha apertado "X" e 
confirmado em algum momento na hora do voto, o programa retornará para o menu principal e você terá que 
colocar os dados novamente para votar (Nome, presidente,gorvernador, senador, deputados ...)
        
E quando finalizado todos os eleitores, o programa será fechado com êxito e seu arquivo .txt estará na pasta onde está o makefile com o nome que foi colocado pelo ADM (Data do dia).
        
    
![N|Solid](https://gitlab.com/pmiranda7410/ep1/raw/master/Read_meEP01/Parte12_FimDeVotos.png)

## Bugs e problemas

Caso, dentro do "Main Menu" do eleitor, depois de escolher uma das duas opções aparecer a mensagem "ERROR: File Open"
Significa que deu Bug na abertura do arquivo .csv dos candidatos.


Formas que Talvez corrija o Bug: 
- Ir na opção "1" do main menu ( Descrição de Candidatos) e digitar, por exemplo, "44" depois apertar Enter.


- *CASO*, apareça os dados do candidato 44 (Paulo Chagas) significa que o programa vai funcionar normalmente e ele realizou
a leitura do arquivo .csv.  (O uso do 44 é um exemplo de um candidato, para forçar a abertura do arquivo).


- *CASO* o erro continue, peço que feche o terminal, dê um *make clean*, *make all* e *make run* e repita essa opção acima como primeira ação.
        
    
        

## Referências
